package com.demo.spring.service;

import java.util.List;
import com.demo.spring.model.Nota;

public interface NotaService {
	
	public void addNota(Nota a);
	public void updateNota(Nota a);
	public List<Nota> listNotas();
	public Nota getNotaById(int id_nota);
	public void removeNota(int id_nota);

}
