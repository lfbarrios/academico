package com.demo.spring.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.demo.spring.dao.AlumnoDAO;
import com.demo.spring.model.Alumno;


@Service
public class AlumnoServiceImpl implements AlumnoService{

	@Autowired
	private AlumnoDAO alumnoDAO;

	public void setAlumnoDAO(AlumnoDAO alumnoDAO) {
		this.alumnoDAO = alumnoDAO;
	}

	@Override
	@Transactional
	public void addAlumno(Alumno a) {
		this.alumnoDAO.addAlumno(a);
	}

	@Override
	@Transactional
	public void updateAlumno(Alumno a) {
		this.alumnoDAO.updateAlumno(a);
	}

	@Override
	@Transactional
	public List<Alumno> listAlumnos() {
		return this.alumnoDAO.listAlumnos();
	}

	@Override
	@Transactional
	public Alumno getAlumnoById(int id_alumno) {
		return this.alumnoDAO.getAlumnoById(id_alumno);
	}

	@Override
	@Transactional
	public void removeAlumno(int id_alumno) {
		this.alumnoDAO.removeAlumno(id_alumno);
	}

}
