package com.demo.spring.service;

import java.util.List;
import com.demo.spring.model.Alumno;

public interface AlumnoService {
	
	public void addAlumno(Alumno a);
	public void updateAlumno(Alumno a);
	public List<Alumno> listAlumnos();
	public Alumno getAlumnoById(int id_alumno);
	public void removeAlumno(int id_alumno);

}
