package com.demo.spring.service;

import java.util.List;
import com.demo.spring.model.Asignatura;

public interface AsignaturaService {

	public void addAsignatura(Asignatura a);
	public void updateAsignatura(Asignatura a);
	public List<Asignatura> listAsignaturas();
	public Asignatura getAsignaturaById(int asignatura_id);
	public void removeAsignatura(int asignatura_id);
	
}
