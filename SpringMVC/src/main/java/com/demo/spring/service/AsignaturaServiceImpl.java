package com.demo.spring.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.demo.spring.dao.AsignaturaDAO;
import com.demo.spring.model.Asignatura;

@Service
public class AsignaturaServiceImpl implements AsignaturaService {

	@Autowired
	private AsignaturaDAO asignaturaDAO;

	public void setAsignaturaDAO(AsignaturaDAO asignaturaDAO) {
		this.asignaturaDAO = asignaturaDAO;
	}

	@Override
	@Transactional
	public void addAsignatura(Asignatura a) {
		this.asignaturaDAO.addAsignatura(a);
	}

	@Override
	@Transactional
	public void updateAsignatura(Asignatura a) {
		this.asignaturaDAO.updateAsignatura(a);
	}

	@Override
	@Transactional
	public List<Asignatura> listAsignaturas() {
		return this.asignaturaDAO.listAsignaturas();
	}

	@Override
	@Transactional
	public Asignatura getAsignaturaById(int asignatura_id) {
		return this.asignaturaDAO.getAsignaturaById(asignatura_id);
	}

	@Override
	@Transactional
	public void removeAsignatura(int asignatura_id) {
		this.asignaturaDAO.removeAsignatura(asignatura_id);
	}

}
