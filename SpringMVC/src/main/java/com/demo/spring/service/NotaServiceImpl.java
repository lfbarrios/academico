package com.demo.spring.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.demo.spring.dao.NotaDAO;
import com.demo.spring.model.Nota;

@Service
public class NotaServiceImpl implements NotaService{
	
	@Autowired
	private NotaDAO notaDAO;

	public void setNotaDAO(NotaDAO notaDAO) {
		this.notaDAO = notaDAO;
	}

	@Override
	@Transactional
	public void addNota(Nota a) {
		this.notaDAO.addNota(a);
	}

	@Override
	@Transactional
	public void updateNota(Nota a) {
		this.notaDAO.updateNota(a);
	}

	@Override
	@Transactional
	public List<Nota> listNotas() {
		return this.notaDAO.listNotas();
	}

	@Override
	@Transactional
	public Nota getNotaById(int id_nota) {
		return this.notaDAO.getNotaById(id_nota);
	}

	@Override
	@Transactional
	public void removeNota(int id_nota) {
		this.notaDAO.removeNota(id_nota);
	}

}
