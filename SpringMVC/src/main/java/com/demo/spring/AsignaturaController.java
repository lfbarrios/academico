package com.demo.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.demo.spring.model.Asignatura;
import com.demo.spring.service.AsignaturaService;

@Controller
public class AsignaturaController {
	
	private AsignaturaService asignaturaService;

	@Autowired(required = true)
	@Qualifier(value = "asignaturaService")
	public void setAsignaturaService(AsignaturaService ps) {
		this.asignaturaService = ps;
	}

	@RequestMapping(value = "/asignaturas", method = RequestMethod.GET)
	public String listAsignaturas(Model model) {
		model.addAttribute("asignatura", new Asignatura());
		model.addAttribute("listAsignaturas", this.asignaturaService.listAsignaturas());
		return "asignatura";
	}

	// For add and update asignatura both
	@RequestMapping(value = "/asignatura/add", method = RequestMethod.POST)
	public String addAsignatura(@ModelAttribute("asignatura") Asignatura p) {
		if (p.getAsignatura_id() == 0) {
			// new asignatura, add it
			this.asignaturaService.addAsignatura(p);
		} else {
			// existing asignatura, call update
			this.asignaturaService.updateAsignatura(p);
		}
		return "redirect:/asignaturas";
	}

	@RequestMapping("/remove/{asignatura_id}")
	public String removeAsignatura(@PathVariable("asignatura_id") int asignatura_id) {
		this.asignaturaService.removeAsignatura(asignatura_id);
		return "redirect:/asignaturas";
	}

	@RequestMapping("/edit/{asignatura_id}")
	public String editAsignatura(@PathVariable("asignatura_id") int asignatura_id, Model model) {
		model.addAttribute("asignatura", this.asignaturaService.getAsignaturaById(asignatura_id));
		model.addAttribute("listAsignaturas", this.asignaturaService.listAsignaturas());
		return "asignatura";
	}
	

}
