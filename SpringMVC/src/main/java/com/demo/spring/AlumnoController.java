package com.demo.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.spring.model.Alumno;
import com.demo.spring.service.AlumnoService;

@Controller
public class AlumnoController {

	private AlumnoService alumnoService;

	@Autowired(required = true)
	@Qualifier(value = "alumnoService")
	public void setAlumnoService(AlumnoService ps) {
		this.alumnoService = ps;
	}

	@RequestMapping(value = "/alumnos", method = RequestMethod.GET)
	public String listAlumnos(Model model) {
		model.addAttribute("alumno", new Alumno());
		model.addAttribute("listAlumnos", this.alumnoService.listAlumnos());
		return "alumno";
	}

	// For add and update alumno both
	@RequestMapping(value = "/alumno/add", method = RequestMethod.POST)
	public String addAlumno(@ModelAttribute("alumno") Alumno p) {
		if (p.getId_alumno() == 0) {
			// new alumno, add it
			this.alumnoService.addAlumno(p);
		} else {
			// existing alumno, call update
			this.alumnoService.updateAlumno(p);
		}
		return "redirect:/alumnos";
	}

	@RequestMapping("/remove/{id_alumno}")
	public String removeAlumno(@PathVariable("id_alumno") int id_alumno) {
		this.alumnoService.removeAlumno(id_alumno);
		return "redirect:/alumnos";
	}

	@RequestMapping("/edit/{id_alumno}")
	public String editAlumno(@PathVariable("id_alumno") int id_alumno, Model model) {
		model.addAttribute("alumno", this.alumnoService.getAlumnoById(id_alumno));
		model.addAttribute("listAlumnos", this.alumnoService.listAlumnos());
		return "alumno";
	}
	
}
