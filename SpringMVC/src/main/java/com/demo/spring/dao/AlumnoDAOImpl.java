package com.demo.spring.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.demo.spring.model.Alumno;

@Repository
public class AlumnoDAOImpl implements AlumnoDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(AlumnoDAOImpl.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addAlumno(Alumno a) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(a);
		logger.info("Alumno guardado correctamente, Detalles del alumno: " + a);
	}

	@Override
	public void updateAlumno(Alumno a) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(a);
		logger.info("Alumno actualizado correctamente, Detalles del alumno: " + a);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Alumno> listAlumnos() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Alumno> alumnosList = session.createQuery("from alumnos").list();
		for (Alumno a : alumnosList) {
			logger.info("Lista de alumnos::" + a);
		}
		return alumnosList;
	}

	@Override
	public Alumno getAlumnoById(int id_alumno) {
		Session session = this.sessionFactory.getCurrentSession();
		Alumno a = (Alumno) session.load(Alumno.class, new Integer(id_alumno));
		logger.info("Alumno obtenido correctamente, Detalles del alumno: " + a);
		return a;
	}

	@Override
	public void removeAlumno(int id_alumno) {
		Session session = this.sessionFactory.getCurrentSession();
		Alumno a = (Alumno) session.load(Alumno.class, new Integer(id_alumno));
		if (null != a) {
			session.delete(a);
		}
		logger.info("Alumno borrado correctamente, Detalles del alumno: " + a);
	}

}
