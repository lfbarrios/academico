package com.demo.spring.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.demo.spring.model.Nota;

@Repository
public class NotaDAOImpl implements NotaDAO{

	private static final Logger logger = LoggerFactory.getLogger(NotaDAOImpl.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addNota(Nota a) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(a);
		logger.info("Nota guardada correctamente, Detalles de la nota: " + a);
	}

	@Override
	public void updateNota(Nota a) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(a);
		logger.info("Nota actualizada correctamente, Detalles de la nota: " + a);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Nota> listNotas() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Nota> notasList = session.createQuery("from notas").list();
		for (Nota a : notasList) {
			logger.info("Lista de notas::" + a);
		}
		return notasList;
	}

	@Override
	public Nota getNotaById(int id_nota) {
		Session session = this.sessionFactory.getCurrentSession();
		Nota a = (Nota) session.load(Nota.class, new Integer(id_nota));
		logger.info("Nota obtenida correctamente, Detalles de la nota: " + a);
		return a;
	}

	@Override
	public void removeNota(int id_nota) {
		Session session = this.sessionFactory.getCurrentSession();
		Nota a = (Nota) session.load(Nota.class, new Integer(id_nota));
		if (null != a) {
			session.delete(a);
		}
		logger.info("Nota borrada correctamente, Detalles de la nota: " + a);
	}
	
}
