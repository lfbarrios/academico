package com.demo.spring.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.demo.spring.model.Asignatura;

@Repository
public class AsignaturaDAOImpl implements AsignaturaDAO {

	private static final Logger logger = LoggerFactory.getLogger(AsignaturaDAOImpl.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addAsignatura(Asignatura a) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(a);
		logger.info("Asignatura guardada correctamente, Detalles de la asignatura: " + a);
	}

	@Override
	public void updateAsignatura(Asignatura a) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(a);
		logger.info("Asignatura actualizada correctamente, Detalles de la asignatura: " + a);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Asignatura> listAsignaturas() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Asignatura> asignaturasList = session.createQuery("from asignaturas").list();
		for (Asignatura a : asignaturasList) {
			logger.info("Lista de asignaturas::" + a);
		}
		return asignaturasList;
	}

	@Override
	public Asignatura getAsignaturaById(int id_asignatura) {
		Session session = this.sessionFactory.getCurrentSession();
		Asignatura a = (Asignatura) session.load(Asignatura.class, new Integer(id_asignatura));
		logger.info("Asignatura obtenida correctamente, Detalles de la asignatura: " + a);
		return a;
	}

	@Override
	public void removeAsignatura(int id_asignatura) {
		Session session = this.sessionFactory.getCurrentSession();
		Asignatura a = (Asignatura) session.load(Asignatura.class, new Integer(id_asignatura));
		if (null != a) {
			session.delete(a);
		}
		logger.info("Asignatura borrada correctamente, Detalles de la asignatura: " + a);
	}

}
