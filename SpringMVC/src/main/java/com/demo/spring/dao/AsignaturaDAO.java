package com.demo.spring.dao;

import java.util.List;
import com.demo.spring.model.Asignatura;

public interface AsignaturaDAO {
	
	public void addAsignatura(Asignatura a);
	public void updateAsignatura(Asignatura a);
	public List<Asignatura> listAsignaturas();
	public Asignatura getAsignaturaById(int id_asignatura);
	public void removeAsignatura(int id_asignatura);

}
