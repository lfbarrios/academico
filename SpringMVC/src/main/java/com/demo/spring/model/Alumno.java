package com.demo.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ALUMNOS")
public class Alumno {

	@Id
	@Column(name="id_alumno")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_alumno;
	
	@Column(name="nombre_alumno")
	private String nombre_alumno;
	
	@Column(name="apellido_alumno")
	private String apellido_alumno;
	
	@Column(name="numero_documento")
	private String numero_documento;
	
	public int getId_alumno() {
		return id_alumno;
	}
	
	public void setId_alumno(int id_alumno) {
		this.id_alumno = id_alumno;
	}
	
	public String getNombre_alumno() {
		return nombre_alumno;
	}
	
	public void setNombre_alumno(String nombre_alumno) {
		this.nombre_alumno = nombre_alumno;
	}
	
	public String getApellido_alumno() {
		return apellido_alumno;
	}
	
	public void setApellido_alumno(String apellido_alumno) {
		this.apellido_alumno = apellido_alumno;
	}
	
	public String getNumero_documento() {
		return numero_documento;
	}
	
	public void setNumero_documento(String numero_documento) {
		this.numero_documento = numero_documento;
	}
	
	@Override
	public String toString() {
		return "Id del alumno: " + id_alumno +
				" - Nombre: " + nombre_alumno +
				" - Apellido: " + apellido_alumno +
				" - Numero de documento: " + numero_documento;
	}
}
