package com.demo.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="NOTAS")
public class Nota {
	
	@Id
	@Column(name="id_nota")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_nota;
	
	@Column(name="descripcion_nota")
	private String descripcion_nota;
	
	public int getId_nota() {
		return id_nota;
	}
	
	public void setId_nota(int id_nota) {
		this.id_nota = id_nota;
	}
	
	public String getDescripcion_nota() {
		return descripcion_nota;
	}
	
	public void setDescripcion_nota(String descripcion_nota) {
		this.descripcion_nota = descripcion_nota;
	}
	
	@Override
	public String toString() {
		return "Id de la nota: " + id_nota +
				" - Descripcion de la nota: " + descripcion_nota;
	}

}
