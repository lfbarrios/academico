package com.demo.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ASIGNATURAS")
public class Asignatura {

	@Id
	@Column(name="id_asignatura")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int asignatura_id;
	
	@Column(name="descripcion_asignatura")
	private String descripcion_asignatura;
	
	public int getAsignatura_id() {
		return asignatura_id;
	}
	
	public void setAsignatura_id(int asignatura_id) {
		this.asignatura_id = asignatura_id;
	}
	
	public String getDescripcion_asignatura() {
		return descripcion_asignatura;
	}
	
	public void setDescripcion_asignatura(String descripcion_asignatura) {
		this.descripcion_asignatura = descripcion_asignatura;
	}

	@Override
	public String toString() {
		return "Id de la asignatura: " + asignatura_id +
				" - Descripcion de la asignatura: " + descripcion_asignatura;
	}
	
}
