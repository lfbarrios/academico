package com.demo.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.spring.model.Nota;
import com.demo.spring.service.NotaService;

@Controller
public class NotaController {
	
	private NotaService notaService;

	@Autowired(required = true)
	@Qualifier(value = "notaService")
	public void setNotaService(NotaService ps) {
		this.notaService = ps;
	}

	@RequestMapping(value = "/notas", method = RequestMethod.GET)
	public String listNotas(Model model) {
		model.addAttribute("nota", new Nota());
		model.addAttribute("listNotas", this.notaService.listNotas());
		return "nota";
	}

	// For add and update nota both
	@RequestMapping(value = "/nota/add", method = RequestMethod.POST)
	public String addNota(@ModelAttribute("nota") Nota p) {
		if (p.getId_nota() == 0) {
			// new nota, add it
			this.notaService.addNota(p);
		} else {
			// existing nota, call update
			this.notaService.updateNota(p);
		}
		return "redirect:/notas";
	}

	@RequestMapping("/remove/{id_nota}")
	public String removeNota(@PathVariable("id_nota") int id_nota) {
		this.notaService.removeNota(id_nota);
		return "redirect:/notas";
	}

	@RequestMapping("/edit/{id_nota}")
	public String editNota(@PathVariable("id_nota") int id_nota, Model model) {
		model.addAttribute("nota", this.notaService.getNotaById(id_nota));
		model.addAttribute("listNotas", this.notaService.listNotas());
		return "nota";
	}

}
